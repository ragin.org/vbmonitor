import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class WebserviceMonitor {
    private static final String[] ENDPOINTS = {
            "https://vbtest3.volkswohl-bund.de/vbl/ws/taa/2024-01/bipro/VBLService_2.4.6.1.12",
            "https://vbnet3.volkswohl-bund.de/vbl/ws/taa/2024-01/bipro/VBLService_2.4.6.1.12"
    };

    public static void main(String[] args) {
        try {
            init();
            System.out.println("Starting...");
            performTestRequests();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }
    public static Properties appProps = new Properties();

    public static void init() {
        try (FileInputStream fileInputStream = new FileInputStream("app.properties")) {
            appProps.load(fileInputStream);
            Thread.sleep(500);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Properties read.");
    }

    private static void performTestRequests() {
        new Thread(() -> {
            while (true) {
                try {
                    Path startPath = Paths.get("C:\\Projects\\vbmonitor\\testdata");
                    Files.walkFileTree(startPath, new SimpleFileVisitor<>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                            if (file.toString().endsWith(".xml")) {
                                String fileContent = new String(Files.readAllBytes(file));
                                for (String endpoint : ENDPOINTS) {
                                    new Thread(() -> {
                                        ResponseDTO rdto = new ResponseDTO();
                                        try {
                                            //System.out.println("File: " + file.toString());
                                            rdto = sendPostRequest(endpoint, fileContent);

                                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            String timestamp = simpleDateFormat.format(new Date());
                                            InputSource src = new InputSource();
                                            src.setCharacterStream(new StringReader(rdto.getrBody()));
                                            String vbKennung = parseResponseBody(rdto.getrBody(), "pm:Kennung");
                                            String vbStatusID = parseResponseBody(rdto.getrBody(), "nachr:StatusID");
                                            //System.out.println(vbKennung + " -> " + vbStatusID);
                                            appendToCSVFile(endpoint, rdto, timestamp, vbKennung, vbStatusID);
                                            System.out.print(".");
                                        } catch (IOException | ParserConfigurationException e) {
                                            e.printStackTrace();
                                        } catch (Exception e) {
                                            System.err.println(rdto.getrBody());
                                            throw new RuntimeException(e);
                                        }
                                    }).start();
                                }
                            }
                            return FileVisitResult.CONTINUE;
                        }
                    });
                    Thread.sleep(Long.parseLong(WebserviceMonitor.appProps.getProperty("monitor.repeat.ms")));
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private static void appendToCSVFile(String endpoint, ResponseDTO rdto, String timestamp, String vbKennung, String vbStatusID) throws IOException {
        FileWriter fileWriter = new FileWriter("response_data.csv", true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(timestamp + ";" + endpoint + ";" + vbStatusID + ";" + vbKennung + ";" + rdto.getrTime() + "\n");
        bufferedWriter.close();
    }


    private static String parseResponseBody(String response, String tagname) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(response)));

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(document);
        transformer.transform(source, result);


        NodeList nodes = document.getElementsByTagName(tagname);
        return nodes.item(0).getTextContent();
    }

    private static ResponseDTO sendPostRequest(String urlString, String requestBody) throws IOException {

        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");

        OutputStream os = con.getOutputStream();
        os.write(requestBody.getBytes());
        os.flush();
        os.close();

        long startTime = System.currentTimeMillis();
        int responseCode = con.getResponseCode();

        long endTime = System.currentTimeMillis();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder respBody = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            respBody.append(inputLine);
        }
        in.close();
        // System.out.println("Endpoint: " + urlString);
        // System.out.println(responseCode + ", Response time: " + (endTime - startTime) + " milliseconds");
        ResponseDTO rdto = new ResponseDTO(respBody.toString(), responseCode, (endTime-startTime));
        if ((endTime - startTime) > Long.parseLong(WebserviceMonitor.appProps.getProperty("monitor.response.max"))) {
            //System.out.println("sending notification mail");
            SendEmail.sendNotificationMail(WebserviceMonitor.appProps, "WS langsam: "+ (endTime - startTime) + " ms", "Response time: " + (endTime - startTime) + " milliseconds");
        }

        return rdto;
    }


}